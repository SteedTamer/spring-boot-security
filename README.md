# spring security学习（SpringBoot2.1.5版本）

### 介绍
spring-boot-security 学习


### 说明

所有例子都已经调通

所有链接数据库的地方都改为了sqlite，代码下载下来直接启动即可


- 注意：示例34,35,36为使用spring cloud oauth2 实现的sso，访问要使用127.0.0.1，不要使用localhost

### 笔记

#### 01-网页登录版入门例子
`配置用户、密码`
`默认登录页面`
`未登录不允许访问`

#### 02-网页登录版入门例子 简单配置类实现01例子
`01所有特性`
`configure基本配置`

#### 03-网页登录版 自定义登录页面
`02所有特性`
`多页面，configure配置细化`
`配置退出页`
`添加测试页面，测试登录后跳转到之前访问页面`

#### 04--网页登录版 其它权限配置和登录成功处理器
`02所有特性`
`登录成功处理器`
`权限验证排除静态文件、指定路径接口`

#### 05-网页登录版 在内存中配置默认用户
`04所有特性`
`内存中配置用户`
`获取用户信息、用户权限信息`

#### 06-网页登录版 方法级别的权限
`05所有特性`
`权限不足处理`
`方法级别权限校验`

#### 07-网页登录版 整合JDBC（sqlite版）
`06所有特性`
`jdbc、sqlite依赖`
`主配置文件配置数据源`
`使用默认数据库结构 org\springframework\security\core\userdetails\jdbc\users.ddl,与org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl同目录`
`configure中配置JdbcUserDetailsManager，包含默认查询语句`

#### 08-网页登录版 整合JDBC（sqlite版） 自定义数据库表结构
`与07类似`
`configure配置调整，使用自定义权限查询service`
`自定义数据库表结构`
`mybatis查询`

#### 09-1-网页登录版  session-data-redis
`08所有特性`
`session-data-redis 并设置session有效期5秒`