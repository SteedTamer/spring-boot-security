package security06.service.impl;

import org.springframework.stereotype.Service;
import security06.service.TestService;

import javax.annotation.security.RolesAllowed;

@Service
public class TestServiceImpl implements TestService {
    @Override
    @RolesAllowed("THREE")
    public String test() {
        System.out.println("service test called");
        return "null";
    }
}
