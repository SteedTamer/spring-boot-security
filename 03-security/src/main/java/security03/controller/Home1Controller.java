package security03.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Home1Controller {

    @GetMapping("/home1")
    public String home(){
        return "home1";
    }
}

