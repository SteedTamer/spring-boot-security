# OAuth2 授权服务
#### 授权并返回授权码
```
get访问
http://localhost:8015/oauth/authorize?client_id=clientId&response_type=code&redirect_uri=http://localhost:8015/
```
#### 根据授权码获取令牌 
```
post访问
http://localhost:8015/oauth/token?grant_type=authorization_code&code=授权码&redirect_uri=http://localhost:8015/&client_id=clientId&client_secret=secret
```
